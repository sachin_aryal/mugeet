import React from 'react'
import { StatusBar } from 'react-native'

import Main from '~main'

const App = () => {
  return (
    <>
      <StatusBar />
      <Main />
    </>
  )
}

export default App
