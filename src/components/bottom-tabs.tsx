import {
  faHome,
  faListMusic,
  faPlus,
  faPodcast,
  faSearch,
} from '@fortawesome/pro-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { HomeScreen } from '~screens/home-screen'
import { LibraryScreen } from '~screens/library-screen'
import { PodcastScreen } from '~screens/podcast-screen'
import { SearchScreen } from '~screens/search-screen'
import { UploadScreen } from '~screens/upload-screen'
import type { RootStackParamsList } from '~types/react-navigation'

const Tab = createBottomTabNavigator<RootStackParamsList>()

export const BottomTabs = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabelStyle: { fontSize: 24 },
          tabBarIcon: ({ size, color }) => (
            <FontAwesomeIcon size={size} color={color} icon={faHome} />
          ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={SearchScreen}
        options={{
          tabBarIcon: ({ size, color }) => (
            <FontAwesomeIcon size={size} color={color} icon={faSearch} />
          ),
        }}
      />
      <Tab.Screen
        name="Upload"
        component={UploadScreen}
        options={{
          tabBarIcon: ({ size, color }) => (
            <FontAwesomeIcon size={size} color={color} icon={faPlus} />
          ),
        }}
      />
      <Tab.Screen
        name="Podcast"
        component={PodcastScreen}
        options={{
          tabBarIcon: ({ size, color }) => (
            <FontAwesomeIcon size={size} color={color} icon={faPodcast} />
          ),
        }}
      />
      <Tab.Screen
        name="Library"
        component={LibraryScreen}
        options={{
          tabBarIcon: ({ size, color }) => (
            <FontAwesomeIcon size={size} color={color} icon={faListMusic} />
          ),
        }}
      />
    </Tab.Navigator>
  )
}
