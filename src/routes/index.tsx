import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import { BottomTabs } from '~components/bottom-tabs'
import { reactNavigationTheme } from '~theme/react-navigation-theme'
import type { RootStackParamsList } from '~types/react-navigation'

const Stack = createNativeStackNavigator<RootStackParamsList>()

export const Routes = () => {
  return (
    <NavigationContainer theme={reactNavigationTheme}>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Root" component={BottomTabs} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
