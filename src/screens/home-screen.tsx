import { faHome } from '@fortawesome/pro-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { Box, Text } from 'native-base'

export const HomeScreen = () => {
  return (
    <Box p="24">
      <FontAwesomeIcon icon={faHome} />
      <Text>Home screen</Text>
    </Box>
  )
}
