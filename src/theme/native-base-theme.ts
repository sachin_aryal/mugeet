import { extendTheme } from 'native-base'

import { COLORS } from '~constants/colors'

export const nativeBaseTheme = extendTheme({
  colors: {
    primary: COLORS.primary,
  },
})
