import { DefaultTheme } from '@react-navigation/native'

import { COLORS } from '~constants/colors'

export const reactNavigationTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: COLORS.primary[600],
  },
}
