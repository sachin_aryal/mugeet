import type { NativeStackScreenProps } from '@react-navigation/native-stack'

export type RootStackParamsList = {
  Root: undefined
  Home: undefined
  Search: undefined
  Upload: undefined
  Podcast: undefined
  Library: undefined
}
export type ScreenProps<TRouteName extends keyof RootStackParamsList> =
  NativeStackScreenProps<RootStackParamsList, TRouteName>
