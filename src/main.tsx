import { NativeBaseProvider } from 'native-base'

import { Routes } from '~routes'
import { nativeBaseTheme } from '~theme/native-base-theme'

const Main = () => {
  return (
    <NativeBaseProvider theme={nativeBaseTheme}>
      <Routes />
    </NativeBaseProvider>
  )
}

export default Main
